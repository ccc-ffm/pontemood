#include <avr/io.h>
#include <util/delay.h>
// vorberechnete tabelle mit den "Steuerwerten" für die PWM, da der µC zu lange braucht die live zu berechnen
#include "fade_table.h"


int main(void) {
	int i=0;
	int j=0;
	int l=0;
	int schalten=0;
	//Alle Pins an PortB als Ausgang definieren
	DDRB = 0b11111111;
	//initialisieren der PWM A1 und A2
	TCCR1A = (1 << WGM10) | (1 << COM1A1) | (1 << COM1B1) | (1 << COM1A0) | (1 << COM1B0);
	TCCR1B = (1 << CS10);
	//initialisieren der PWM 2
	TCCR2  = (1 << WGM20) | (1 << WGM21) | (1 << CS20) | (1 << COM21) | (1 << COM20);
	//Initialisierungssequenz (zum testen)
	OCR1A = 255;
	OCR1B = 0;
	OCR2  = 0;
	_delay_ms(500);
	OCR1A = 0;
	OCR1B = 255;
	OCR2  = 0;
	_delay_ms(500);
	OCR1A = 0;
	OCR1B = 0;
	OCR2  = 255;
	_delay_ms(500);
	OCR1A = 255;
	OCR1B = 255;
	OCR2  = 255;
	_delay_ms(500);
	//busyloop
	while(1) {
		for (l=0; l<=255; l++) {
			_delay_ms(50);
			//i = (unsigned char) 255 * (1-cos((255.-l)/255.*3.141592/2) );
			//j = (unsigned char) 255 * (1-cos((    l)/255.*3.141592/2) );
			i = fade_in[l];
			j = fade_out[l];
//			i=l;
//			j=255-i;
			if (l==255) {
				schalten++;
				j=0;
				i=255;
			}
			
			switch (schalten) {
				case 0:
					OCR1A=j;
					OCR1B=i;
					OCR2=0;
				break;
				case 1:
					OCR1A=i;
					OCR1B=0;
					OCR2=j;
				break;
				case 2:
					OCR1A=0;
					OCR1B=j;
					OCR2=i;
				break;
			/*	case 3:	
					OCR1A=0;
					OCR1B=0;
					OCR2 =0;
					break;*/
				default:
					schalten = 0;
				break;
			}
		}
	}
	return 0;
}
