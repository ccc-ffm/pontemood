#! /usr/bin/python

import math


fadein = ''
fadeout = ''
for i in range ( 16 ):
	for j in range ( 16 ):
		l = 16*i+j
		fadein  += '0x%02X, ' % ( 255 * (1-math.cos((255.-l)/255.*3.141592/2) ) )
		fadeout += '0x%02X, ' % ( 255 * (1-math.cos((     l)/255.*3.141592/2) ) )
	fadein  += '\n\t'
	fadeout += '\n\t'

text = '\
unsigned char fade_in[%d] = {\n\t%s\n};\n\n\
unsigned char fade_out[%d] = {\n\t%s\n};\n\n' % (256, fadein[:-4], 256, fadeout[:-4])


f = open ( 'fade_table.h', 'w' )
f.write ( text )
f.close ( )


